import threading, subprocess, time
from datetime import datetime

def print_pesan(args=True, host='none'):
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
    file = open('report-monitor.csv', "a")
    if (args == "UP" or args == "DOWN"):
        result_detail = dt_string + ';' + host + ';' + args
        print(str(result_detail))
        file.write(result_detail + '\n')
    else:
        print('Alamat ip salah')
    file.close()


def check_host(ip):
    status, result = subprocess.getstatusoutput("ping -n 2 " + ip)
    if (status == 0):
        print_pesan(args='UP', host=ip)
    else:
        print_pesan(args='DOWN', host=ip)


def run_check_host():
    file = open('hostt.cfg', 'r')
    ips = file.readlines()
    threads = []
    t1 = time.perf_counter()
    print('\nmonitoring di mulai....\n')
    for x in ips:
        ip = [x]
        t = threading.Thread(target=check_host, args=ip)
        t.start()
        threads.append(t)

    for t in threads:
        t.join()

    t2 = time.perf_counter()
    print(f"\nmonitoring akan selesai dalam waktu : {round(t2 - t1, 2)} detik")

WAIT_TIME_SECOND = 10
ticker = threading.Event()
if __name__ == "__main__":
    try:
        while not ticker.wait(WAIT_TIME_SECOND):
            run_check_host()
    except :
        ticker.clear()
        print('\nclose program')
